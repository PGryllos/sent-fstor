from functools import partial
import pytest
import pytest_benchmark
import shutil
from subprocess import call


@pytest.fixture()
def command():
    def create_command(folder_count=3, folder_size=20, file_size=1):
        folders = ','.join(['{},{}'.format(i, folder_size) for i in range(1, folder_count+1)])
        cmd = 'python  data_management.py  -l /tmp/test_master-ds  -f {}  -s {}'.format(folders, file_size)
        return cmd
    yield create_command
    call(['rm', '-rf', '/tmp/test_master-ds'])

    
def test_3_small_folders(benchmark, command):
    exit_code = benchmark(partial(call, command(folder_size=5).split('  ')))
    assert exit_code == 0


def test_3_medium_folders(benchmark, command):
    exit_code = benchmark(partial(call, command(folder_size=30).split('  ')))
    assert exit_code == 0


def test_3_bigger_folders(benchmark, command):
    exit_code = benchmark(partial(call, command(folder_size=100).split('  ')))
    assert exit_code == 0


def test_5_bigger_folders(benchmark, command):
    exit_code = benchmark(partial(call, command(folder_count=5, folder_size=100).split('  ')))
    assert exit_code == 0
