import argparse
from subprocess import call
import hashlib
import os
from functools import partial
from queue import Queue
from random import randint
from time import sleep
from threading import Event
import sys
from threading import Thread


def generate_random_alphanumerics(msg_queue, stop_event):
    while True:
        if stop_event.is_set():
            break
        if not msg_queue.full():
            # sha1 produces 40 hexdigit messages, so each message is 400 chars
            msg_queue.put(hashlib.sha1(bytes(randint(1, 10000))).hexdigest() * 10)
        else:
            sleep(0.01)


def create_file(msg_queue, abs_path=None, size=None):
    with open(abs_path, 'a') as f:
        produced_size = 1
        while produced_size < size:
            line_size = randint(200, 1000)
            line_size = line_size if line_size + produced_size <= size else size - produced_size

            # the denominator must equal the length of messages put into the queue
            for i in range(int(line_size // 400)):
                f.write(msg_queue.get())
                msg_queue.task_done()
            if line_size % 400:
                f.write('{}\n'.format(msg_queue.get()[:int(line_size % 400)]))
                msg_queue.task_done()
            else:
                f.write('\n')
            produced_size += line_size

    
def populate_folder(msg_queue, folder=None, size=None, file_size=None, extend=False):
    if extend:
        start = len(os.listdir(folder))
        file_size = round(os.path.getsize('{}/file_0'.format(folder)))
    else:
        start = 0
    for i in range(start, int(size // file_size) + start):
        create_file(msg_queue, '{}/file_{}'.format(folder, i), file_size)

    if size % file_size:
        create_file(msg_queue, '{}/file_{}'.format(folder, i+1), size % file_size)


if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-f', '--folders', type=str,
        help='folder names and the corresponding folder sizes in the form <name1>,<size1>,<name2>,<size2>')
    ap.add_argument('-l', '--location', default='master-ds', type=str,
        help='path to the directory where Dataset will be written')
    ap.add_argument('-s', '--size', default=1, type=int, help='requested file size in MB')
    ap.add_argument('--update', action='store_true', help='if used then extension happens instead of creation')
    ap.add_argument('--backup', default='', type=str,
        help='backup dataset found under directory specified with location under target directory')

    main_dir = ap.parse_args().location
    backup_dir = ap.parse_args().backup
    if backup_dir:
        if not os.path.exists(backup_dir):
            os.makedirs(backup_dir)
            count = 1
        else:
            count = len(os.listdir(backup_dir)) + 1
        call(['tar', '-zcvf', '{}/sent_ds_BAK_{}.tar.gz'.format(backup_dir, count), main_dir])
        sys.exit()

    file_size = ap.parse_args().size
    # to not be confused! folders var is not representing folders, it is a list with all the information put in the
    # folders argument
    folders = ap.parse_args().folders.split(',')
    folders_sizes = [('{}/{}'.format(main_dir, folders[i]), float(folders[i+1])) for i in range(0, len(folders), 2)]

    for f, s in folders_sizes:
        if not os.path.exists(f):
            os.makedirs(f)
        assert file_size < float(s)
    
    msg_queue = Queue(maxsize=100)
    try:
        stop_event = Event()
        producer = Thread(target=partial(generate_random_alphanumerics, msg_queue, stop_event))
        producer.deamon = True
        producer.start()

        for f, s in folders_sizes:
            if ap.parse_args().update:
                consumer = Thread(target=partial(populate_folder, msg_queue, f, s*10**6, extend=True))
            else:
                consumer = Thread(target=partial(populate_folder, msg_queue, f, s*10**6, file_size*10**6))
            consumer.start()
            consumer.join()
        stop_event.set()
    except KeyboardInterrupt:
        sys.exit()
