DSman
--

Simple command line tool written in python for generating, storing, updating and keeping backups
of datasets on a local machine.

## usage
```
usage: data_management.py [-h] [-f FOLDERS] [-l LOCATION] [-s SIZE] [--update]
                          [--backup BACKUP]

optional arguments:
  -h, --help            show this help message and exit
  -f FOLDERS, --folders FOLDERS
                        folder names and the corresponding folder sizes in the
                        form <name1>,<size1>,<name2>,<size2>
  -l LOCATION, --location LOCATION
                        path to the directory where Dataset will be written
  -s SIZE, --size SIZE  requested file size in MB
  --update              if used then extension happens instead of creation
  --backup BACKUP       backup dataset found under directory specified with
                        location under target directory
```

### examples
To generate a new dataset you have to choose a location, a prefered file size and folder names with their required sizes.
```shell
python data_management.py --location master-ds -s 2 --folders A,20,B,53
```
This will create two folders under master-ds called A and B containing 2MB files with random content and having a total size of
20MB and 53MB respectively.

```shell
python data_management.py --location master-ds --folders A,30,B,13 --update
```
This will extend the existing folders A and B found under master-ds with extra files, of size equal to the
size of the existing files, with extra content of 30MB and and 13MB respectively.

```shell
python data_management.py --location master-ds --backup bak
```
This will backup all the content of the folder master-bs by placing it in folder bak in gzip format.


###notes
* Make sure to use absolute paths in case you are running the script from a different directory.
* I haven't tested with floats as file size or folder size, but it should work without problems.


## implementation details

### tools
The tool has been tested in python3.4 and python2.7. Only standard libraries
have been used so running it in unix envinronments should be as easy as just invoking the script with the
appropriate arguments.

The `backup` functionality makes use of the unix utility `tar`. That will impose a problem for the specific
funtionality on Windows envronments. Containerizing the functionality can be a viable workaround that I have
partially created but cannot deliver due to some difficulties relating to proper data volumes manipulation
but the idea is that an image can be built containing the module and the environment (.e.g based on python:3.5-alpine)
and as long as `--location` and `--backup` volumes are mounted upon running of the container an entrypoint script
can take care of calling the data manipulation script with the appopriate arguments.

using the script for doing a backup then would be look like

```shell
docker run --rm -v master-ds:/master-ds -v bak:/bak <image name> --backup
```


## tests
A small set of performance tests have been added that can be run like that
```shell
pytest -v
```
(you have to run this command from the main directory of the repo and not from inside the tests folder)

Those tests do not offer coverage as they are not unit tests but at least can serve as integrations tests and peformance metrics. My goal with those tests
was to showcase that time rises linearly with the requested folder sizes.

here is an example run in my computer

```shell
tests/test_performance.py::test_3_small_folders PASSED
tests/test_performance.py::test_3_medium_folders PASSED
tests/test_performance.py::test_3_bigger_folders ^C


--------------------------------------------------------------------------- benchmark: 3 tests --------------------------------------------------------------------------
Name (time in s)              Min                Max               Mean            StdDev             Median               IQR            Outliers(*)  Rounds  Iterations
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
test_3_small_folders       1.8354 (1.0)       2.6222 (1.0)       2.3598 (1.0)      0.3231 (1.0)       2.4853 (1.0)      0.4306 (1.0)              1;0       5           1
test_3_medium_folders     13.3113 (7.25)     14.8012 (5.64)     14.0595 (5.96)     0.5491 (1.70)     14.0663 (5.66)     0.7011 (1.63)             2;0       5           1
test_3_bigger_folders     44.7144 (24.36)    51.3872 (19.60)    48.6414 (20.61)    2.9020 (8.98)     49.4012 (19.88)    5.0324 (11.69)            1;0       5           1
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------

(*) Outliers: 1 Standard Deviation from Mean; 1.5 IQR (InterQuartile Range) from 1st Quartile and 3rd Quartile.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! KeyboardInterrupt !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
to show a full traceback on KeyboardInterrupt use --fulltrace
/usr/lib/python3.4/subprocess.py:491: KeyboardInterrupt
====================================================================== 2 passed in 421.70 seconds ======================================================================
```

I had to kill the third test because it my disk is full.


## system architecture
This module implements a simple producer-consumer schema. The whole system consists of two threads (and the main thread running the cli); a producer that generates pseudo random strings of constant size and places them in a fixed size message queue and a consumer that uses those strings to construct lines
of pseuro randomly varying lengths that get written into files.

![Alt text](system_architecture.png?raw=true "System Architecture")

### why not async?
Since most the operations performed by the system are I/O operations this could seem like a good case for an async application where requests for writes
are queued and nothing gets blocked while those writes are happening. But that is a totally false perception for the case of filesystem operations. First
because most systems don't support non blocking I/O operations to the filesystem and second because this logic cannot offer good performance in the case
of filesystem operations. I will explain that in the next section.

### why not many threads?
One could also think that spawning multiple threads (e.g. one thread per file or one thread per folder) could speed up things but again this is far from
true for most systems. That's because most systems do not offer real parallel I/O operations to the filesystem. Additionally in the case that our system
operates on a machine with hdd disk spawning multiple threads will cause a huge amount of overhead since each thread will be moving the head of the disk
(which in this case is the bottleneck of the performance) to a different place to start a new write operation.

Using one thread for writing and tuning the producer (size of queue, size of messages) so that there is no chance that a consumer will be waiting for
messages to be generated is the best performing architecture.

Practically for every folder a new consumer thread is spawned after the previous has terminated.


### lines are written in chunks
One could not see why lines are written in chunks. It would be easier implementation wise to construct the whole line in memory and write
to the file as a whole. But since our lines can have arbitrary sizes (I have set the maximum length to 1000 but in a real case senario that could be millions or even more) it would be dangerous to store the whole line in memory.


## Would the implementation change once the data size gets larger? If so, how?
The provided implementation does not depend on the size of the requested dataset. As long as there is enough disk space the procedure can
keep writing data with maximum throughput. To achieve maximum throughput some parameters may have to be tuned like the length of
the messages in the queue and the maximum length of the queue according to our system's writing to disk speed.


## How would you handle a data size that becomes too large to hold on a single machine on local disks?
Probably HDFS could be a good option, the current system could be writing to hdfs instead of a local filesystem with some adaptations.


## Would there be libraries or tools that you could use to help out? If so, which ones and how could they help?
In the case of hdfs, I would probably use hdfs api library provided for python that would enable me to make the appropriate calls.
